# MTA-Communication-Enhancement
This is a tool that can be used to communicate between the server and the client.
It's main purpose is to automate and enhancement trigger[Client/Server]Event functions with more functionality and easier to use.

### Challenges
* The current trigger*events can be complex for some people to use.
* CallBack functionality is not directly available.
* Sending trigger events before the client has been loaded happens too often. This message will not be received.
* Can't pass over arguments to the callback function without losing them or making copies
* Make it async/database callback ready. 

### Syntax
The syntax readme about the tool can be found in the folder: 
* [documentation/syntax/readme.md](documentation/syntax/readme.md)


### Examples
The examples can be found in: 
* [documentation/examples/readme.md](documentation/examples/readme.md)


### Testing
Use the folder `testing-area` for testing the examples of this tool.
* [Testing-area](testing-area)


### Set-up
* [Set-up for your own resources](documentation/set-up/readme.md)
