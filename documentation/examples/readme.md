
# Examples 

This file is not required to make use of this tool. It just contains some examples.


* [Syntax](../syntax)
* [Testing-area](../../testing-area)
* [Set-up for your own resources](../set-up)

<br/><br/><br/>


## Calling from clientside to serverside

#### CLIENT
```lua
callServer("hello")
```

#### SERVER
```lua
function hello ()
	outputChatBox("Hello client!")
end
```

#### TODO
* Just run the code



<br/><br/><br/>



## Calling from serverside to clientside

#### SERVER
```lua
addCommandHandler("callclient", function (player) 
	
	-- An addCommandHandler is needed, because the client hasn't loaded it's scripts yet.
	callClient(player, "hello")
	
end, false, false)
```


#### CLIENT
```lua
function hello ()
	outputChatBox("Hello server!")
end
```

#### TODO
* /callclient OR /callClient


<br/><br/><br/>



## Passing arguments


#### CLIENT
```lua
callServer("passingArguments", "This is a passed argument.")
```

#### SERVER
```lua
function passingArguments (argument)
	outputChatBox(argument)
end
```


#### TODO
* Just run the code



<br/><br/><br/>


## Callback
	
The first and ONLY the first function you passed as argument will be the callback function.


#### CLIENT
```lua
callServer(
	"callbackMe", 
	"argument", 
	function (argument) -- < This is the callback function
		outputChatBox(argument) 
	end
)
```

#### SERVER
```lua
function callbackMe (argument)
	return argument .. " < I looked at it :)"
end
```

#### TODO
* Just run the code



<br/><br/><br/>



## Callback + internal arguments
	
This argument is not send to the other side(client/server), but kept and passed over on the same side.


#### CLIENT
```lua
callServer(
	"callbackMe", 
	
	--------------------------------
	-- arguments that are send over
	
	"argument", 
	
	--
	--------------------------------
	
	function (internalArgument, argument) -- < This is the callback function.
	
		outputChatBox(internalArgument)
		outputChatBox(argument) 
		
	end,
	
	--------------------------------
	-- arguments that are not send over
	
	"internalArgument" -- < internal argument
	
	--
	--------------------------------
)
```

#### SERVER
```lua
function callbackMe (argument)
	return argument .. " < I looked at it :D"
end
```

#### TODO
* Just run the code


<br/><br/><br/>


#### Warning
Do not pass arguments with nil values.

For example:
```lua
local argument1 = "normal argument"
local internalArgument1 = 1
local internalArgument2 = nil
local internalArgument3 = nil

callServer(
	"callbackMe",
	
	argument1, -- < argument 1
	
	function (arg1, arg2, arg3, arg4) 
		print(arg1) -- print: 1
		print(arg2) -- print: "normal argument"
		print(arg3) -- print: nil
		print(arg4) -- print: nil
	end, 
	
	internalArgument1, -- < internal arguments
	internalArgument2, 
	internalArgument3
)
```

```
(1, nil, nil) = (1)
```

Arguments that are have a nil value at the END of the argument-list will not be detected.




This will work just fine:
```lua
local internalArgument1 = nil
local internalArgument2 = nil
local internalArgument3 = true
```

```
(nil, nil, true) = (nil, nil, true)
```

<br/><br/><br/>

## Calling a (remote) player from clientside

#### CLIENT

```lua
function smile (player)
	
	outputChatBox((isElement(player) and getPlayerName(player) or "[unknown]") .. " has send you a: :)")
	
	local x, y, z = getElementPosition(localPlayer)
	setElementPosition(localPlayer, x, y, z + 100)
	
end
addRemoteClientAccessPoint(smile) -- < This function allows other clients to call this function.


---------------------------------------
--                                   --
function getPlayerFromPartialName(name)
    local name = name and name:gsub("#%x%x%x%x%x%x", ""):lower() or nil
    if name then
        for _, player in ipairs(getElementsByType("player")) do
            local name_ = getPlayerName(player):gsub("#%x%x%x%x%x%x", ""):lower()
            if name_:find(name, 1, true) then
                return player
            end
        end
    end
end
-- Author: TAPL
-- https://wiki.multitheftauto.com/wiki/GetPlayerFromPartialName
--                                   --
---------------------------------------


addCommandHandler("smile", 
function (cmd, playerName) 
	local player = getPlayerFromPartialName(playerName)
	if player then
		outputChatBox("Sending smile!")
		callRemoteClient(player, "smile", player)
	else
		outputChatBox("Can't find player!")
	end
end)
```

Sending a smile to a player.

#### TODO
* Type the command: /smile `[playerName]` 

<br/><br/><br/>

## Calling a (remote) player from clientside with await

#### CLIENT
```lua
function ping (timeNow)
	outputChatBox("\nPING! Returned after: " .. (getTickCount() - timeNow) .. " ms")
	
	
	return "And returning the whole process back!"
end
addRemoteClientAccessPoint(ping) -- < This function allows other clients to call this function.


local timeNow = getTickCount()

outputChatBox("\n\n\nStart making a connection validation! 0 ms")
callRemoteClientAwait(localPlayer, "ping", 
	timeNow,
	function (message)
		outputChatBox("\nPONG! " .. message .. " After: " .. (getTickCount() - timeNow) .. " ms")
	end
)
```

This example will show you how long it takes before a communication is complete:
`[START] your pc > server > your pc [END]`

But also the use of a callback function with you as target client:
`[START] your pc > server > your pc > server > your pc [END]`

If localPlayer is replaced with a remote player:
`[START] your pc > server > remote client > server > your pc [END]`

[NOTE] This code is executed immediately and it is important to use the await version here. Clients can only receiving messages when they have been loaded all pieces of the code. The await version will make sure that the client will receive the message, after he/she has loaded.



#### TODO
* Run the code. 

<br/><br/><br/>

## Calling and waiting for the not loaded player/players.


#### SERVER
```lua
addEventHandler("onPlayerJoin", root, 
function ()
	
	callClientAwait(source, "testCallClientAwait")
	
end)
```


#### CLIENT
```lua
function testCallClientAwait ()
	outputChatBox("Yes this works!")
end
```


#### TODO
* Run the code. 
* JOIN the server or RECONNECT if you are already inside.


#### THIS DOES NOT WORK:
```lua
-----------------
addEventHandler("onPlayerJoin", root, 
function ()
	
	-- !!!!!!!!!!!!!!
	callClient(source, "testCallClientAwait") -- NOT WORKING !!!!!!!!!!!!!!!!!!!!!!!!!
	-- !!!!!!!!!!!!!!
	
	--[[ 
		The client hasn't loaded it's scripts yet!
		Use: callClientAwait
	]]
end)
-----------------
```


<br/><br/><br/>


#### Decide when to callback



##### [NOTE]: There is a cleaner system that will clean messages data after a while.
* The callback might possibility might be removed after 10 minutes
* The callback possibility is certainly removed after 30 minutes

So do not wait too long with it! (Do not be a granny!)



#### CLIENT
```lua
local timeNow = getTickCount() --[[
	"This function returns amount of time that your system has been running in milliseconds."
	https://wiki.multitheftauto.com/wiki/GetTickCount
]]

outputChatBox("Send!")

callServer("decideWhenToCallback", 
	"argument1",
	function (argument1)
		outputChatBox("Return! After milliseconds: " .. (getTickCount() - timeNow))
		outputChatBox("This is also send back: " .. tostring(argument1))
	end
)
```


#### SERVER
```lua
function decideWhenToCallback (argument1)
	
	-- Create a callback function + table as it's container.
	local callbackFunction, callbackContainer = createCallback() 
	
	
	-- Sneaky editing the argument a little bit 
	argument1 = argument1 .. " + something :D sneaky"
	
	
	-- The `callbackFunction` can be used to resume the process. You can pass arguments to it and those will be available to you on the other side.
	setTimer(callbackFunction, 2000, 1, argument1)
	
	
	-- Return the container. This will tell the system that you are using a callback function of your own.
	return callbackContainer
end
```


#### TODO
* Just run the code



<br/><br/><br/>




## Whitelist

#### TODO [A]
* Open file [sync/sync_shared.lua](../../sync/sync_shared.lua)
* Change the value of the variable: `functionWhitelistState` to > true < to enable.

#### SERVER
```lua
callClientAwait(root, "checkWhitelist1")

callClientAwait(root, "checkWhitelist2")
```

#### CLIENT
```lua
outputChatBox("Is the whitelist enabled? " .. (isWhitelistEnabled() and "YES" or "NO, enabled it in the file sync/sync_shared.lua"))

function checkWhitelist1 ()
	outputChatBox("Function 1 has been called")
end

function checkWhitelist2 ()
	outputChatBox("Function 2 has been called")
end
addToWhitelist(checkWhitelist2) -- Add the function checkWhitelist2 to the whitelist.
```

#### TODO [B]
* Run the code


#### RESULT
Only the second function `checkWhitelist2` will be able to be called from serverside. The function `checkWhitelist1` is not added to the whitelist.
