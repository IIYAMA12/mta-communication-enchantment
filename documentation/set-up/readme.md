# Set-up

* [Examples](../examples)
* [Syntax](../syntax)
* [Testing-area](../../testing-area)

0. Pick a resource you want to give this tool.

1. Copy the folder [sync](..) to the selected resource. (At the same filepath level as your meta.xml)


2. Copy these lines to the meta.xml of the selected resource.

```xml
	<!-- set-up files : START -->
		<!-- Keep the files in order -->
		<script src="sync/sync_shared.lua" type="shared" /><!-- FIRST -->
		<script src="sync/sync_s.lua" type="server" /><!-- SECOND -->
		<script src="sync/sync_c.lua" type="client" /><!-- THIRD -->
	<!-- set-up files : END -->		
	
	
	<!-- \\\\\\ YOUR SCRIPTS BELOW /////// -->
	<!--  \\\\\\------------------///////  -->
	<!--   \\\\\\----------------///////   -->
	<!--    \\\\\\--------------///////    -->
	<!--     \\\\\\------------///////     -->
```

Make sure that your scripts are placed lower than these lines.

3. Finished!