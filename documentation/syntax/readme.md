# Syntax

* [Examples](../examples)
* [Testing-area](../../testing-area)
* [Set-up for your own resources](../set-up)

<br/><br/><br/>



## Clientside functions

#### Syntax:
```	
bool callServer (callFunctionName string [, argument1, argument2, ...  ] [, callbackFunction function [, argument1 (internal), argument2 (internal), ...]]  )
```	

#### Required arguments
* `callFunctionName` This is the function you are going to call on serverside.

#### Optional arguments
* arguments [...]
* `callbackFunction` This is the function you are going to call back, FROM serverside, BACK TO clientside.
* internal arguments [...] These arguments will not be send to the otherside, but they will be available in the callback function.

#### Returns
* A success state: boolean

<br/>

#### Syntax:
```	
bool callRemoteClient (sendTo player, callFunctionName string [, argument1, argument2, ...  ] [, callbackFunction function [, argument1 (internal), argument2 (internal), ...]]  )
```

#### Required arguments
* `sendTo` this argument is used to define the receiver of the call. This must be a player. (localPlayer is allowed)

#### Optional arguments
* arguments [...]
* `callbackFunction` This is the function you are going to call back. [START] clientside (localPlayer) > serverside > clientside (sendTo) > serverside > clientside (localPlayer) [END]
* internal arguments [...] These arguments will not be send to the otherside, but they will be available in the callback function.

#### Returns
* A success state: boolean

<br/>


#### Syntax:
```	
bool callRemoteClientAwait (sendTo player, callFunctionName string [, argument1, argument2, ...  ] [, callbackFunction function [, argument1 (internal), argument2 (internal), ...]]  )
```

This function waits with sending until the player that is in the `sendTo` argument has been loaded. The success sending chance is now secured.

#### Required arguments
* `sendTo` this argument is used to define the receiver of the call. This must be a player. (localPlayer is allowed)

#### Optional arguments
* arguments [...]
* `callbackFunction` This is the function you are going to call back. [START] clientside (localPlayer) > serverside > clientside (sendTo) > serverside > clientside (localPlayer) [END]
* internal arguments [...] These arguments will not be send to the otherside, but they will be available in the callback function.


#### Returns
* A success state: boolean

<br/>

#### Syntax:
```
bool addRemoteClientAccessPoint(function)
```

#### Required arguments
* The function you want to make call able for remote clients.

#### Returns
* A success state: boolean

<br/>

#### Syntax:
```
bool isInRemoteClientAccessPoint (function)
```

#### Required arguments
* The function you want to check if it is call able by remote clients.

#### Returns
* A success state: boolean


<br/>

#### Syntax:
```
bool removeRemoteClientAccessPoint(function)
```

#### Required arguments
* The function you do not want to make call able for remote clients. Only required when the function `addRemoteClientAccessPoint` has been used.

#### Returns
* A success state: boolean


<br/><br/><br/>


## Serverside functions

#### Syntax:
```
bool callClient (sendTo player/table/root , callFunctionName string [, argument1, argument2, ...  ] [, callbackFunction function [, argument1 (internal), argument2 (internal), ...]] )
```

#### Required arguments
* `sendTo` this argument is used to define the receiver of the call. This can be a player, the root element(All players) or a table which contains players.
* `callFunctionName` This is the function you are going to call on clientside.

#### Optional arguments
* arguments [...]
* `callbackFunction` This is the function you are going to call back, FROM clientside, BACK TO serverside.
* internal arguments [...] These arguments will not be send to the otherside, but they will be available in the callback function.

#### Returns
* A success state: boolean

<br/>


#### Syntax:
```
bool, index int = callClientAwait (send to player/table/root , callFunctionName string [, argument1, argument2, ...  ] [, callbackFunction function [, argument1 (internal), argument2 (internal), ...]] )
```

This function waits with sending until the player(collection) that is in the `sendTo` argument has been loaded. The success sending chance is now secured.

#### Required arguments
* `sendTo` this argument is used to define the receiver of the call. This can be a player, the root element(All players) or a table which contains players.
* `callFunctionName` This is the function you are going to call on clientside.

#### Optional arguments
* arguments [...]
* `callbackFunction` This is the function you are going to call back, FROM clientside, BACK TO serverside.
* internal arguments [...] These arguments will not be send to the otherside, but they will be available in the callback function.


#### Returns
* A success state.
* The index of the await message. Which you can use as an identifier for the message. If there are no players are waiting for their messages, then index will be false.


<br/><br/><br/>

## Predefined variables
[NOTE] Just as with the triggerClientEvent, the predefined variable `client` is available on serverside while using the callServer function. (Because triggerClientEvent is used behind the scenes.)
This variable is also available in the callback function that is used on callClient or callClientAwait.

<br/><br/><br/>

## Shared variables

#### Location:
Located in the [sync/sync_shared.lua](../../sync/sync_shared.lua) file.

Whitelist:

```lua
local functionWhitelistState = false
```
This variable enables a white list, which you can use to block function-calls that are not on the whitelist. By default this whitelist is disabled for a quick start.

<br/>

Secure calls:
```lua
local secureCallStatus = false
```
This variable makes all function calls in to your own code "protected call's". All errors + warnings will be catched and ignored, making sure that the communication system keeps doing what it should do. Do only enable this setting if you are not developing, because developing without errors is a no go.
More information about how this works: [pcall](https://www.lua.org/pil/8.4.html) 


<br/><br/><br/>

## Shared functions

<br/>

Whitelist:

#### Syntax
```
bool addToWhitelist(function)
```
Add a function to the whitelist.

<br/>

#### Syntax
```
bool removeFromWhitelist(function)
```
Remove a function from the whitelist.

<br/>

#### Syntax
```
bool isInWhitelist(function)
```
Is a function in the whitelist?

<br/>

#### Syntax
```
bool isWhitelistEnabled()
```
Check if the whitelist is enabled.

<br/><br/><br/>

Callback:


#### Syntax
```
callbackFunction function, callbackContainer table = createCallback()
```
This function has only one purpose. It allows you to make a temporary callback function which will call your actual callback function, if the target is assigned. See function: setCallbackTarget
The tool itself set's the target automatically once you returned the callbackContainer.


Benefits
* Local functions will not be deleted in case they are longer needed.
* The order of the `callback call` and `target assignment` doesn't matter, as long as it happens within the same execution.

<br/>

#### Syntax
```
bool setCallbackTarget (callbackContainer table, target function)
```
Assign the target to the callback function.

<br/>

#### Syntax
```
bool destroyCallback(callbackContainer table)
```
Probably never going to be used, but there you go, you can destroy callback tables as well.


<br/><br/><br/>

Other functions:

#### Syntax
```
bool callNextFrame(function [, arguments])
```
With this function you can move a function call to the next frame. It can be used for heavy workload or just simple call a function asynchronous.

It can also be used on serverside. But there will be a 50 millisecond timer used. If you call this function multiple times in a single execution, it will not create multiple timers but use only 1.

#### Required arguments
* The function you want to call asynchronous.

#### Optional arguments
* Arguments you want to pass.